﻿using Microsoft.EntityFrameworkCore;
using Krestic.App.Domain.Designs;

namespace Krestic.App.Infrastructure.Market.Queries
{
    public class MarketQueryDbContext : DbContext
    {
        public DbSet<Design> Designs { get; set; }

        protected MarketQueryDbContext() { }
        public MarketQueryDbContext(DbContextOptions<MarketQueryDbContext> contextOptions) : base(contextOptions) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var design = modelBuilder.Entity<Design>().ToTable("Design");
            design.HasKey(x => x.Id);
            design.Property(x => x.DesignerId).IsRequired();
            design.Property(x => x.Title);
            design.HasOne(x => x.Designer).WithMany().HasForeignKey(x => x.DesignerId);
        }
    }
}
