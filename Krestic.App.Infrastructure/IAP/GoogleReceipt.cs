﻿namespace Krestic.App.Infrastructure.IAP
{
    public class GoogleReceipt
    {
        public string json { get; set; }
        public string signature { get; set; }
    }
}
