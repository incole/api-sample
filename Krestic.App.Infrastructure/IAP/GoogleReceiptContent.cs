﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Unicode;

namespace Krestic.App.Infrastructure.IAP
{

    public class GoogleReceiptContent
    {
        public string orderId { get; set; }
        public string packageName { get; set; }
        public string productId { get; set; }
        public long purchaseTime { get; set; }
        public int purchaseState { get; set; }
        public string purchaseToken { get; set; }
    }
}
