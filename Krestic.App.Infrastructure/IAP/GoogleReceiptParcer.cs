﻿using System.Text.Json;

namespace Krestic.App.Infrastructure.IAP
{
    public static class GoogleReceiptParcer
    {
        public static GoogleReceiptContent Parce(string rawdata)
        {
            //rawdata = rawdata.Replace("\\\\\"", "\"");

            //if (rawdata.IndexOf("\\\\\"") > 0)
            //{ 
            //    rawdata = rawdata.Replace("\\\"", "\"");
            //    rawdata = rawdata.Replace(@"\\", @"\");
            //}

            if (rawdata.Contains("Payload"))
                rawdata = JsonSerializer.Deserialize<UnityPayload>(rawdata).Payload;

            var receipt = JsonSerializer.Deserialize<GoogleReceipt>(rawdata);
            var order = JsonSerializer.Deserialize<GoogleReceiptContent>(receipt.json);

            return order;
        }
    }
}
