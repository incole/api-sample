﻿using Microsoft.EntityFrameworkCore;
using Krestic.App.Domain.Metrics;

namespace Krestic.App.Infrastructure.Metrics
{
    public class MetricsDbContext : DbContext
    {
        public DbSet<AppExceptionMessage> AppExceptionMessages { get; set; }

        protected MetricsDbContext() { }
        public MetricsDbContext(DbContextOptions<MetricsDbContext> contextOptions) : base(contextOptions) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var appExceptionMessage = modelBuilder.Entity<AppExceptionMessage>().ToTable("AppExceptionMessage");
            appExceptionMessage.HasKey(x => x.Id);
            appExceptionMessage.Property(x => x.Id).ValueGeneratedNever();
        }
    }
}
