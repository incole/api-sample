﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Krestic.App.Domain.Metrics;
using Krestic.App.Domain;

namespace Krestic.App.Infrastructure.Metrics
{
    public class AppExceptionMessageRepository : IRepository<AppExceptionMessage>
    {
        private readonly MetricsDbContext _context;

        public AppExceptionMessageRepository(MetricsDbContext dbContext)
        {
            _context = dbContext;
        }

        public async Task<AppExceptionMessage> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _context.AppExceptionMessages
                .SingleOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public async Task SaveAsync(AppExceptionMessage value)
        {
            var entry = _context.Entry(value);
            if (entry.State == EntityState.Detached)
                await _context.AppExceptionMessages.AddAsync(value);

            await _context.SaveChangesAsync();
        }

    }
}
