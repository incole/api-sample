using Krestic.App.Domain;
using Krestic.App.Domain.Accounts;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Infrastructure.Accounts
{
    public class DesignCodeService
    {
        private readonly AccountsDbContext _dbContext;
        private readonly IRepository<Account> _accountRepository;
        private readonly IRepository<DesignCode> _designCodeRepository;

        public DesignCodeService(AccountsDbContext dbContext, IRepository<Account> accountRepository, IRepository<DesignCode> designCodeRepository)
        {
            _dbContext = dbContext;
            _accountRepository = accountRepository;
            _designCodeRepository = designCodeRepository;
        }

        public async Task ConsumeDesignCode(Account account, DesignCode code, CancellationToken cancellationToken = default)
        {
            using var transaction = _dbContext.Database.BeginTransaction();

            try
            {
                account.ConsumeCode(code);

                await _accountRepository.SaveAsync(account);
                await _designCodeRepository.SaveAsync(code);
                
                transaction.Commit();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
