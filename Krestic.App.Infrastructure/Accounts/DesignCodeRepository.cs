﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using Krestic.App.Domain.Accounts;
using Krestic.App.Domain;

namespace Krestic.App.Infrastructure.Accounts
{
    public class DesignCodeRepository : IRepository<DesignCode>
    {
        private readonly AccountsDbContext _context;

        public DesignCodeRepository(AccountsDbContext dbContext)
        {
            _context = dbContext;
        }

        public async Task<DesignCode> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _context.DesignCodes
                .Include(x => x.Design)
                .SingleOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public async Task SaveAsync(DesignCode value)
        {
            var entry = _context.Entry(value);
            if (entry.State == EntityState.Detached)
                await _context.DesignCodes.AddAsync(value);

            await _context.SaveChangesAsync();
        }

    }
}
