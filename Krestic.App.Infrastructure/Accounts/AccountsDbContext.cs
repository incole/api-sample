﻿using Microsoft.EntityFrameworkCore;
using Krestic.App.Domain.Accounts;

namespace Krestic.App.Infrastructure.Accounts
{
    public class AccountsDbContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Designer> Designers { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<DesignCode> DesignCodes { get; set; }
        public DbSet<DesignAccess> DesignAccess { get; set; }

        protected AccountsDbContext() { }

        public AccountsDbContext(DbContextOptions<AccountsDbContext> contextOptions) : base(contextOptions) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var accounts = modelBuilder.Entity<Account>().ToTable("Account");
            accounts.HasKey(x => x.Id);
            accounts.Property(x => x.Email).IsRequired();
            accounts.Property(x => x.Password).IsRequired();
            accounts.Property(x => x.CreateDate).IsRequired();
            accounts.HasMany(x => x.DesignAccess).WithOne().HasForeignKey(x => x.AccountId);
            accounts.HasMany(x => x.Purchases).WithOne().HasForeignKey(x => x.AccountId);

            var session = modelBuilder.Entity<Session>().ToTable("Session");
            session.HasKey(x => x.Id);
            session.Property(x => x.ExpireDate).IsRequired();

            var designer = modelBuilder.Entity<Designer>().ToTable("Designer");
            designer.HasKey(x => x.Id);
            designer.Property(x => x.AccountId).IsRequired();
            designer.Property(x => x.Title).IsRequired();

            var purchase = modelBuilder.Entity<Purchase>().ToTable("Purchase");
            purchase.HasKey(x => x.Id);
            purchase.Property(x => x.Id).ValueGeneratedNever();

            var designAccess = modelBuilder.Entity<DesignAccess>().ToTable("DesignAccess");
            designAccess.HasKey(x => x.Id);
            designAccess.Property(x => x.Id).ValueGeneratedNever();

            var designCode = modelBuilder.Entity<DesignCode>().ToTable("DesignCode");
            designCode.HasKey(x => x.Id);
            designCode.Property(x => x.Code).IsRequired();
            designCode.HasIndex(x => x.Code).IsUnique();
            designCode.HasOne(x => x.Design).WithMany().HasForeignKey(x => x.DesignId);
        }
    }
}
