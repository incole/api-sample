﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class SessionQueryHandler : IRequestHandler<SessionQuery, SessionQueryResult>
    {
        private AccountsDbContext _accountsDbContext;

        public SessionQueryHandler(AccountsDbContext accountsDbContext)
        {
            _accountsDbContext = accountsDbContext;
        }

        public async Task<SessionQueryResult> Handle(SessionQuery request, CancellationToken cancellationToken)
        {
            var session = await _accountsDbContext.Sessions.SingleOrDefaultAsync(x => x.Id == request.Key);

            if (session == null)
                return null;

            return new SessionQueryResult()
            {
                Id = session.Id,
                ClientId = session.ClientId,
                AccountId = session.AccountId,
                ExpireDate = session.ExpireDate,
                IsActive = session.IsActive
            };
        }
    }
}
