﻿using System;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class PurchaseView
    {
        public Guid Id { get; set; }
        public Guid AccountId { get; set; }
        public Guid DesignId { get; set; }
        public decimal Amount { get; set; }
        public decimal PlatformFee { get; set; }
        public decimal Fee { get; set; }
        public DateTime Date { get; set; }
        public string Platform { get; set; }
        public string ReceiptId { get; set; }
        public string ReceiptRaw { get; set; }
        public string ReceiptHash { get; set; }
    }
}