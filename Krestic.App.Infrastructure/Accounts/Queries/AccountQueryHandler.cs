﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Krestic.App.Domain.Accounts;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class AccountQueryHandler : IRequestHandler<AccountQuery, AccountView>
    {
        private AccountsDbContext _accountsDbContext;

        public AccountQueryHandler(AccountsDbContext accountsDbContext)
        {
            _accountsDbContext = accountsDbContext;
        }

        public async Task<AccountView> Handle(AccountQuery request, CancellationToken cancellationToken)
        {
            Account account = null;

            if (request.Id.HasValue)
                account = await _accountsDbContext.Accounts.SingleOrDefaultAsync(x => x.Id == request.Id);
            else if (request.Email != null)
                account = await _accountsDbContext.Accounts.SingleOrDefaultAsync(x => x.Email == request.Email.ToLower());

            if (account == null)
                return null;

            return new AccountView()
            {
                Id = account.Id,
                CreateDate = account.CreateDate,
                Email = account.Email,
                FirstName = account.FirstName,
                IsActive = account.IsActive,
                LastName = account.LastName,
                Password = account.Password
            };
        }
    }
}
