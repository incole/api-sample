﻿using System;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class AccountView
    {
        public Guid Id { get; set; }
        public bool IsActive { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreateDate { get; set; }
        public string Password { get; set; }
    }
}
