﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Krestic.App.Infrastructure.Accounts;
using Krestic.App.Infrastructure.Accounts.Queries;
using Krestic.App.WebApi.Contract;
using Krestic.App.WebApi.Contract.Accounts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Infrastructure.Designs.Queries
{
    public class AccountDesignsQueryHandler : IRequestHandler<AccountDesignsQuery, Page<DesignView>>
    {
        private AccountsDbContext _dbContext;

        public AccountDesignsQueryHandler(AccountsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<DesignView>> Handle(AccountDesignsQuery request, CancellationToken cancellationToken)
        {
            //from design in _accountsDbContext.Designs
            //join purchase in _accountsDbContext.Purchases
            //on Design.Id equals purchase.DesignId into DesignPurchases
            //from designPurchase in DesignPurchases.DefaultIfEmpty()
            //where !Design.IsHidden
            //where
            //     (designPurchase != null) ||
            //     (Design.DesignerId == designerId)
            //select new AccountDesignView


            var query = _dbContext.Accounts
                .Include(x => x.DesignAccess)
                .ThenInclude(x => x.Design)
                .Where(x => x.Id == request.AccountId)
                .SelectMany(x => x.DesignAccess)
                .Where(x => x.IsActive)
                .Select(x => new DesignView
                {
                    Id = x.Design.Id,
                    Title = x.Design.Title,
                    Floss = new DesignView.FlossData
                    {
                        Vendor = x.Design.FlossVendor,
                        Count = x.Design.FlossCount,
                    },
                    Canvas = new DesignView.CanvasData
                    {
                        Type = x.Design.CanvasType,
                        Color = x.Design.CanvasColor,
                        Count = x.Design.CanvasCount,
                        Height = x.Design.CanvasHeight,
                        Width = x.Design.CanvasWidth,
                    },
                    Icon = x.Design.Icon,
                    Designer = new DesignerView
                    {
                        Id = x.Design.DesignerId,
                        Title = x.Design.Designer.Title
                    },
                    Price = x.Design.Price,
                    StitchDimensions = new StitchDimensions
                    {
                        Width = x.Design.StitchWidth,
                        Height = x.Design.StitchHeight,
                        TotalStitches = x.Design.TotalStitches
                    }
                });

            return new Page<DesignView>
            {
                Total = query.Count(),
                Items = await query.Skip((request.Page - 1) * request.Limit)
                                   .Take(request.Limit)
                                   .ToArrayAsync()
            };
        }
    }

}
