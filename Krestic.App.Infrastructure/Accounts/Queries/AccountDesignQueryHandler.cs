﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Krestic.App.Infrastructure.Accounts;
using Krestic.App.Infrastructure.Accounts.Queries;
using Krestic.App.WebApi.Contract;
using Krestic.App.WebApi.Contract.Accounts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Infrastructure.Designs.Queries
{
    public class AccountDesignQueryHandler : IRequestHandler<AccountDesignQuery, DesignView>
    {
        private AccountsDbContext _dbContext;

        public AccountDesignQueryHandler(AccountsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<DesignView> Handle(AccountDesignQuery request, CancellationToken cancellationToken)
        {
            return _dbContext.Accounts
                .Include(x => x.DesignAccess)
                .ThenInclude(x => x.Design)
                .Where(x => x.Id == request.AccountId)
                .SelectMany(x => x.DesignAccess)
                .Where(x => x.IsActive)
                .Where(x => x.DesignId == request.DesignId)
                .Select(x => new DesignView
                {
                    Id = x.Design.Id,
                    Title = x.Design.Title,
                    Floss = new DesignView.FlossData
                    {
                        Vendor = x.Design.FlossVendor,
                        Count = x.Design.FlossCount,
                    },
                    Canvas = new DesignView.CanvasData
                    {
                        Type = x.Design.CanvasType,
                        Color = x.Design.CanvasColor,
                        Count = x.Design.CanvasCount,
                        Height = x.Design.CanvasHeight,
                        Width = x.Design.CanvasWidth,
                    },
                    Icon = x.Design.Icon,
                    Designer = new DesignerView
                    {
                        Id = x.Design.DesignerId,
                        Title = x.Design.Designer.Title
                    },
                    Price = x.Design.Price,
                    StitchDimensions = new StitchDimensions
                    {
                        Width = x.Design.StitchWidth,
                        Height = x.Design.StitchHeight,
                        TotalStitches = x.Design.TotalStitches
                    }
                })
                .FirstOrDefault();
        }
    }

}
