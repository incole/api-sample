﻿using MediatR;
using System;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class PurchaseQuery : IRequest<PurchaseView>
    {
        public string Hash { get; }

        public PurchaseQuery(string hash)
        {
            Hash = hash;
        }
    }
}
