﻿using Krestic.App.WebApi.Contract.Accounts;
using System;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class DesignCodeQueryView
    {
        public Guid Id { get; set; }
        public DesignView Design { get; set; }
        public string Code { get; set; }
        public bool IsActive { get; set; }
        public bool IsConsumed { get; set; }
    }
}
