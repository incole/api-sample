﻿using MediatR;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class DesignCodeQuery : IRequest<DesignCodeQueryView>
    {
        public string Code { get; init; }

        public DesignCodeQuery(string code)
        {
            Code = code;
        }
    }
}
