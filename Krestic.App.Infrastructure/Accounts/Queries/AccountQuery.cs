﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class AccountQuery: IRequest<AccountView>
    {
        public Guid? Id { get; set; }
        public string Email { get; set; }

        public AccountQuery(Guid? id = null, string email = null)
        {
            Id = id;
            Email = email;
        }
    }
}
