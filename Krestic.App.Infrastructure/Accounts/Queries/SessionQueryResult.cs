﻿using System;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class SessionQueryResult
    {
        public Guid Id { get; set; }
        public Guid AccountId { get; set; }
        public string ClientId { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool IsActive { get; set; }
    }
}
