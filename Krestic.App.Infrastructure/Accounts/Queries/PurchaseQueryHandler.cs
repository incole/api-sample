﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class PurchaseQueryHandler : IRequestHandler<PurchaseQuery, PurchaseView>
    {
        private AccountsDbContext _dbContext;

        public PurchaseQueryHandler(AccountsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PurchaseView> Handle(PurchaseQuery request, CancellationToken cancellationToken)
        {
            var purchase = await _dbContext.Purchases.SingleOrDefaultAsync(x => x.ReceiptHash == request.Hash);

            if (purchase == null)
                return null;

            return new PurchaseView
            {
                Id = purchase.Id,
                DesignId = purchase.DesignId,
                AccountId = purchase.AccountId,
                Amount = purchase.Amount,
                Fee = purchase.Fee,
                Platform = purchase.Platform,
                PlatformFee = purchase.PlatformFee,
                ReceiptHash = purchase.ReceiptHash,
                ReceiptId = purchase.ReceiptId,
                ReceiptRaw = purchase.ReceiptRaw,
                Date = purchase.Date
            };
        }
    }
}
