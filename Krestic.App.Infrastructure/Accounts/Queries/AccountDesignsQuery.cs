﻿using MediatR;
using Krestic.App.WebApi.Contract;
using Krestic.App.WebApi.Contract.Accounts;
using System;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class AccountDesignsQuery: IRequest<Page<DesignView>>
    {
        public Guid AccountId { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; }

        public AccountDesignsQuery(Guid accountId, int page = 1, int limit = 100)
        {
            AccountId = accountId;
            Page = page;
            Limit = limit;
        }
    }
}
