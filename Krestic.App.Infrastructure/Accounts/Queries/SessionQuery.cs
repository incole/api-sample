﻿using MediatR;
using System;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class SessionQuery : IRequest<SessionQueryResult>
    {
        public Guid Key { get; set; }

        public SessionQuery(Guid key)
        {
            Key = key;
        }
    }
}
