﻿using MediatR;
using Krestic.App.WebApi.Contract.Accounts;
using System;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class AccountDesignQuery : IRequest<DesignView>
    {
        public Guid AccountId { get; set; }
        public Guid DesignId { get; set; }

        public AccountDesignQuery(Guid accountId, Guid designId)
        {
            AccountId = accountId;
            DesignId = designId;
        }
    }
}
