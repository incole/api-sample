﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Krestic.App.WebApi.Contract.Accounts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Infrastructure.Accounts.Queries
{
    public class DesignCodeQueryHandler : IRequestHandler<DesignCodeQuery, DesignCodeQueryView>
    {
        private AccountsDbContext _dbContext;

        public DesignCodeQueryHandler(AccountsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<DesignCodeQueryView> Handle(DesignCodeQuery request, CancellationToken cancellationToken)
        {
            return await _dbContext.DesignCodes
                .Include(x => x.Design)
                .Where(x => x.Code == request.Code.ToLower())
                .Where(x => !x.Design.IsHidden)
                .Where(x => x.Design.IsListed)
                .Select(x => new DesignCodeQueryView
                {
                    Id = x.Id,
                    Code = x.Code,
                    IsActive = x.IsActive,
                    IsConsumed = x.ReceiveDate.HasValue,
                    Design = new DesignView
                    {
                        Id = x.Design.Id,
                        Title = x.Design.Title,
                        Floss = new DesignView.FlossData
                        {
                            Vendor = x.Design.FlossVendor,
                            Count = x.Design.FlossCount,
                        },
                        Canvas = new DesignView.CanvasData
                        {
                            Type = x.Design.CanvasType,
                            Color = x.Design.CanvasColor,
                            Count = x.Design.CanvasCount,
                            Height = x.Design.CanvasHeight,
                            Width = x.Design.CanvasWidth,
                        },
                        Icon = x.Design.Icon,
                        Designer = new DesignerView
                        {
                            Id = x.Design.DesignerId,
                            Title = x.Design.Designer.Title
                        },
                        Price = x.Design.Price,
                        StitchDimensions = new StitchDimensions
                        {
                            Width = x.Design.StitchWidth,
                            Height = x.Design.StitchHeight,
                            TotalStitches = x.Design.TotalStitches
                        }
                    }
                })
                .SingleOrDefaultAsync();
        }
    }
}
