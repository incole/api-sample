﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Krestic.App.Domain.Accounts;
using Krestic.App.Domain;

namespace Krestic.App.Infrastructure.Accounts
{
    public class AccountRepository : IRepository<Account>
    {
        private readonly AccountsDbContext _context;

        public AccountRepository(AccountsDbContext dbContext)
        {
            _context = dbContext;
        }

        public async Task<Account> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Accounts
                .Include(x => x.DesignAccess)
                .Include(x => x.Purchases)
                .SingleOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public async Task SaveAsync(Account value)
        {
            var entry = _context.Entry(value);
            if (entry.State == EntityState.Detached)
                await _context.Accounts.AddAsync(value);

            await _context.SaveChangesAsync();
        }

    }
}
