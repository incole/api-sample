﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Krestic.App.Domain.Accounts;
using Krestic.App.Domain;

namespace Krestic.App.Infrastructure.Accounts
{
    public class SessionRepository : IRepository<Session>
    {
        private readonly AccountsDbContext _context;

        public SessionRepository(AccountsDbContext dbContext)
        {
            _context = dbContext;
        }

        public async Task<Session[]> All(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<Session> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Sessions
                .SingleOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public async Task SaveAsync(Session value)
        {
            var entry = _context.Entry(value);
            if (entry.State == EntityState.Detached)
                await _context.Sessions.AddAsync(value);

            await _context.SaveChangesAsync();
        }

    }
}
