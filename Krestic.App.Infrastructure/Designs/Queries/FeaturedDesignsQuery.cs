﻿using MediatR;
using Krestic.App.WebApi.Contract;
using Krestic.App.WebApi.Contract.Designs;

namespace Krestic.App.Infrastructure.Designs.Queries
{
    public class FeaturedDesignsQuery : IRequest<Page<DesignSimpleView>>
    {
        public int Page { get; set; }
        public int Limit { get; set; }

        public FeaturedDesignsQuery(int page, int limit)
        {
            Page = page;
            Limit = limit;
        }
    }
}