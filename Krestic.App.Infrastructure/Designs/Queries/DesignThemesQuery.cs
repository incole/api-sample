﻿using MediatR;
using Krestic.App.WebApi.Contract.Designs;

namespace Krestic.App.Infrastructure.Designs
{
    public class DesignThemesQuery : IRequest<string[]> { }
}