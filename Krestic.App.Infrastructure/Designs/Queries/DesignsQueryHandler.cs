﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Krestic.App.WebApi.Contract;
using Krestic.App.WebApi.Contract.Designs;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Infrastructure.Designs.Queries
{
    public class DesignsQueryHandler : IRequestHandler<DesignsQuery, Page<DesignSimpleView>>
    {
        private readonly DesignsDbContext _dbContext;

        public DesignsQueryHandler(DesignsDbContext designsDbContext)
        {
            _dbContext = designsDbContext;
        }

        public async Task<Page<DesignSimpleView>> Handle(DesignsQuery request, CancellationToken cancellationToken)
        {
            var designs = _dbContext.Designs
                .Include(x => x.Designer)
                .Where(x => x.IsPublished)
                .Where(x => x.IsListed)
                .Where(x => !x.IsHidden);

            if (request.Designer != null)
                designs = designs.Where(x => x.Designer.Title == request.Designer);

            if (request.Theme != null)
                designs = designs.Where(x => x.Theme == request.Theme);

            if (request.SearchString != null)
                designs = designs.Where(x => EF.Functions.ILike(
                    x.Title + " " + 
                    x.Collection + " " + 
                    x.Designer.Title + " " +
                    (x.Theme != null ? x.Theme : "") + " " +
                    (x.Keywords != null ? x.Keywords : ""),
                $"%{request.SearchString.ToLower()}%"));

            switch (request.Order)
            {
                case DesignsListOrder.Common:
                    designs = designs.OrderBy(x => x.Id);
                    break;

                case DesignsListOrder.Random:
                    designs = designs.OrderBy(x => Guid.NewGuid());
                    break;

                case DesignsListOrder.PublishDate:
                    designs = designs.OrderByDescending(x => x.PublishDate);
                    break;
            }

            var page = new Page<DesignSimpleView>
            {
                Total = designs.Count(),
                Items = await designs.Select(x => new DesignSimpleView
                {
                    Id = x.Id,
                    Title = x.Title,
                    Collection = x.Collection,
                    Icon = x.Icon,
                    Designer = new DesignerView
                    {
                        Id = x.DesignerId,
                        Title = x.Designer.Title
                    },
                    Price = x.Price
                })
                .Skip((request.Page - 1) * request.Limit)
                .Take(request.Limit)
                .ToArrayAsync()
            };

            return page;
        }
    }
}
