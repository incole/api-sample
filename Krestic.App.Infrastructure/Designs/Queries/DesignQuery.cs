﻿using MediatR;
using Krestic.App.WebApi.Contract.Designs;
using System;

namespace Krestic.App.Infrastructure.Designs
{
    public class DesignQuery : IRequest<DesignCompleteView>
    {
        public DesignQuery(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}