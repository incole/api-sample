﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Krestic.App.WebApi.Contract;
using Krestic.App.WebApi.Contract.Designs;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Infrastructure.Designs.Queries
{
    public class FeaturedDesignsQueryHandler : IRequestHandler<FeaturedDesignsQuery, Page<DesignSimpleView>>
    {
        private readonly DesignsDbContext _dbContext;

        public FeaturedDesignsQueryHandler(DesignsDbContext designsDbContext)
        {
            _dbContext = designsDbContext;
        }

        public async Task<Page<DesignSimpleView>> Handle(FeaturedDesignsQuery request, CancellationToken cancellationToken)
        {
            var designs = _dbContext.Designs
                .Include(x => x.Designer)
                .Where(x => x.IsPublished)
                .Where(x => x.IsListed)
                .Where(x => !x.IsHidden)
                .Where(x => x.Collection == "Featured")
                .OrderBy(x => Guid.NewGuid());

            var page = new Page<DesignSimpleView>
            {
                Total = designs.Count(),
                Items = await designs.Select(x => new DesignSimpleView
                {
                    Id = x.Id,
                    Title = x.Title,
                    Collection = x.Collection,
                    Icon = x.Icon,
                    Designer = new DesignerView
                    {
                        Id = x.DesignerId,
                        Title = x.Designer.Title
                    },
                    Price = x.Price
                })
                .Skip((request.Page - 1) * request.Limit)
                .Take(request.Limit)
                .ToArrayAsync()
            };

            return page;
        }
    }
}
