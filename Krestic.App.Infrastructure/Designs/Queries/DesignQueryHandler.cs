﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Krestic.App.WebApi.Contract.Designs;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Infrastructure.Designs.Queries
{
    public class DesignQueryHandler : IRequestHandler<DesignQuery, DesignCompleteView>
    {
        private readonly DesignsDbContext _designsDbContext;

        public DesignQueryHandler(DesignsDbContext designsDbContext)
        {
            _designsDbContext = designsDbContext;
        }

        public async Task<DesignCompleteView> Handle(DesignQuery request, CancellationToken cancellationToken)
        {
            var design = await _designsDbContext.Designs
                .Include(x => x.Designer)
                .Include(x => x.Images)
                .Where(x => x.IsListed)
                .Where(x => !x.IsHidden)
                .SingleOrDefaultAsync(x => x.Id == request.Id);

            if (design == null)
                return null;

            return new DesignCompleteView
            {
                Id = design.Id,
                Title = design.Title,
                Collection = design.Collection,
                Icon = design.Icon,
                Designer = new DesignerView
                {
                    Id = design.DesignerId,
                    Title = design.Designer.Title
                },
                Price = design.Price,
                Floss = new DesignCompleteView.FlossData
                {
                    Vendor = design.FlossVendor,
                    Count = design.FlossCount,
                },
                Canvas = new DesignCompleteView.CanvasData
                {
                    Type = design.CanvasType,
                    Color = design.CanvasColor,
                    Count = design.CanvasCount,
                    Height = design.CanvasHeight,
                    Width = design.CanvasWidth,
                },
                HasBackStitches = design.HasBackStitches,
                HasHalfStitches = design.HasHalfStitches,
                HasQuarterStitches = design.HasQuarterStitches,
                HasFrenchKnots = design.HasFrenchKnots,
                HasPetitStitches = design.HasPetitStitches,
                HasBlends = design.HasBlends,
                StitchDimensions = new StitchDimensions
                {
                    Width = design.StitchWidth,
                    Height = design.StitchHeight,
                    TotalStitches = design.TotalStitches
                },
                Images = design.Images.OrderBy(x=>x.Order)
                                      .Select(x => new DesignImageView
                                      {
                                          Id = x.Id,
                                          Url = x.Url
                                      })
                                      .ToArray()
            };
        }
    }
}
