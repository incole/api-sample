﻿using MediatR;
using Krestic.App.WebApi.Contract.Designs;
using System;

namespace Krestic.App.Infrastructure.Designs
{
    public class DesignImagesQuery : IRequest<DesignImageView[]>
    {
        public Guid DesignId { get; set; }

        public DesignImagesQuery(Guid designId)
        {
            DesignId = designId;
        }
    }
}