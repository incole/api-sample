﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Krestic.App.WebApi.Contract.Designs;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Infrastructure.Designs.Queries
{
    public class DesignImagesQueryHandler : IRequestHandler<DesignImagesQuery, DesignImageView[]>
    {
        private readonly DesignsDbContext _designsDbContext;

        public DesignImagesQueryHandler(DesignsDbContext designsDbContext)
        {
            _designsDbContext = designsDbContext;
        }

        public async Task<DesignImageView[]> Handle(DesignImagesQuery request, CancellationToken cancellationToken)
        {
            return await _designsDbContext.Images
                .Where(x => x.DesignId == request.DesignId)
                .OrderBy(x => x.Order)
                .Select(x => new DesignImageView
                { 
                    Id = x.Id,
                    Url = x.Url
                })
                .ToArrayAsync();
        }
    }
}
