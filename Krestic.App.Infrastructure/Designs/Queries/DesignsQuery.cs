﻿using MediatR;
using Krestic.App.WebApi.Contract;
using Krestic.App.WebApi.Contract.Designs;
using System;

namespace Krestic.App.Infrastructure.Designs.Queries
{
    public class DesignsQuery: IRequest<Page<DesignSimpleView>>
    {
        public int Page { get; set; }
        public int Limit { get; set; }
        public string Designer { get; set; }
        public string Theme { get; set; }
        public string SearchString { get; set; }
        public DateTime? FromPublishDate { get; set; }
        public DesignsListOrder Order { get; set; }

        public DesignsQuery(int page, int limit, string designer, string theme, string searchString, DateTime? fromPublishDate)
        {
            Limit = limit;
            Page = page;
            Designer = designer;
            SearchString = searchString;
            Theme = theme;
            FromPublishDate = fromPublishDate;
            Order = DesignsListOrder.Random;
        }
    }
}