﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Krestic.App.WebApi.Contract.Designs;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Infrastructure.Designs.Queries
{
    public class DesignThemesQueryHandler : IRequestHandler<DesignThemesQuery, string[]>
    {
        private DesignsDbContext _dbContext;

        public DesignThemesQueryHandler(DesignsDbContext accountsDbContext)
        {
            _dbContext = accountsDbContext;
        }

        public async Task<string[]> Handle(DesignThemesQuery request, CancellationToken cancellationToken)
        {
            return await _dbContext.Themes
                .Select(x => x.Title)
                .OrderBy(x => x)
                .ToArrayAsync();
        }
    }
}
