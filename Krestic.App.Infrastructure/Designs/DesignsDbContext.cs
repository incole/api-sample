﻿using Microsoft.EntityFrameworkCore;
using Krestic.App.Domain.Designs;

namespace Krestic.App.Infrastructure.Designs
{
    public class DesignsDbContext : DbContext
    {
        public DbSet<Design> Designs { get; set; }
        public DbSet<DesignImage> Images { get; set; }
        public DbSet<DesignTheme> Themes { get; set; }

        protected DesignsDbContext() { }
        public DesignsDbContext(DbContextOptions<DesignsDbContext> contextOptions) : base(contextOptions) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var design = modelBuilder.Entity<Design>().ToTable("Design");
            design.HasKey(x => x.Id);
            design.Property(x => x.DesignerId).IsRequired();
            design.Property(x => x.Title);
            design.HasOne(x => x.Designer).WithMany().HasForeignKey(x => x.DesignerId);
            design.HasMany(x => x.Images).WithOne().HasForeignKey(x => x.DesignId);

            var designImage = modelBuilder.Entity<DesignImage>().ToTable("DesignImage");
            designImage.HasKey(x => x.Id);
            designImage.Property(x => x.DesignId).IsRequired();
            designImage.Property(x => x.Url).IsRequired();
            designImage.Property(x => x.Order).IsRequired();

            var theme = modelBuilder.Entity<DesignTheme>().ToTable("DesignTheme");
            theme.HasKey(x => x.Title);
        }
    }
}
