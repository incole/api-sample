﻿namespace Krestic.App.WebApi.Contract
{
    public class Page<T> where T: class
    {
        public T[] Items { get; set; }
        public int Total { get; set; }
    }
}