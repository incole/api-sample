﻿using System;

namespace Krestic.App.WebApi.Contract.Designs
{
    public class DesignImageView
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
    }
}