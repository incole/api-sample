﻿namespace Krestic.App.WebApi.Contract.Designs
{
    public enum DesignsListOrder
    {
        Common,
        Rating,
        PublishDate,
        Random
    }
}