﻿using System;

namespace Krestic.App.WebApi.Contract.Designs
{
    public class DesignerView
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}