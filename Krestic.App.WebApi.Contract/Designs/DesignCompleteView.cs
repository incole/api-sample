﻿using System;

namespace Krestic.App.WebApi.Contract.Designs
{
    public class DesignCompleteView
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Collection { get; set; }
        public float ReviewScore { get; set; }
        public int ReviewCount { get; set; }
        public FlossData Floss { get; set; }
        public CanvasData Canvas { get; set; }
        public decimal Price { get; set; }
        public bool HasBackStitches { get; set; }
        public bool HasHalfStitches { get; set; }
        public bool HasQuarterStitches { get; set; }
        public bool HasPetitStitches { get; set; }
        public bool HasFrenchKnots { get; set; }
        public bool HasBlends { get; set; }
        public StitchDimensions StitchDimensions { get; set; }
        public DesignerView Designer { get; set; }
        public string Icon { get; set; }
        public DesignImageView[] Images { get; set; }

        public class FlossData
        {
            public string Vendor { get; set; }
            public int Count { get; set; }
        }

        public class CanvasData
        {
            public int Count { get; set; }
            public string Type { get; set; }
            public string Color { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
        }
    }
}