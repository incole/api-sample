﻿using System;
using System.Collections.Generic;

namespace Krestic.App.WebApi.Contract.Designs
{
    public class DesignSimpleView
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Collection { get; set; }
        public decimal Price { get; set; }
        public DesignerView Designer { get; set; }
        public string Icon { get; set; }
    }
}