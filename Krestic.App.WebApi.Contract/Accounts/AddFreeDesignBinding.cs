﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Krestic.App.WebApi.Contract.Accounts
{
    public class AddFreeDesignBinding
    {
        public Guid DesignId { get; set; }

        public AddFreeDesignBinding(Guid designId)
        {
            DesignId = designId;
        }
    }

}