﻿using System;

namespace Krestic.App.WebApi.Contract.Accounts
{
    public class AccountView
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid? DesignerId { get; set; }
        public DateTime CreateDate { get; set; }
    }

}