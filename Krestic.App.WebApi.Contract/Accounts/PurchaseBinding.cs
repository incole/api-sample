﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Krestic.App.WebApi.Contract.Accounts
{
    public class PurchaseBinding
    {
        public Guid DesignId { get; set; }
        public string Payload { get; set; }

        public PurchaseBinding(Guid designId, string payload)
        {
            DesignId = designId;
            Payload = payload ?? throw new ArgumentNullException(nameof(payload));
        }
    }
}