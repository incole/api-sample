﻿using System.ComponentModel.DataAnnotations;

namespace Krestic.App.WebApi.Contract.Accounts
{
    public class AuthorizationBinding
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
