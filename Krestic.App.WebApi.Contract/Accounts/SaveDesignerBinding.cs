﻿using System.ComponentModel.DataAnnotations;

namespace Krestic.App.WebApi.Contract.Accounts
{
    public class SaveDesignerBinding
    {
        public string Title { get; set; }
    }
}
