﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Krestic.App.WebApi.Contract.Accounts
{
    public class RegistrationBinding
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
