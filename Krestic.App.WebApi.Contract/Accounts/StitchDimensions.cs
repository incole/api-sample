﻿namespace Krestic.App.WebApi.Contract.Accounts
{
    public class StitchDimensions
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int TotalStitches { get; set; }
    }
}