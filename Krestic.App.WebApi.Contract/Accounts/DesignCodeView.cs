﻿using System;

namespace Krestic.App.WebApi.Contract.Accounts
{
    public class DesignCodeView
    {
        public Guid Id { get; set; }
        public DesignView Design { get; set; }
        public string Code { get; set; }
    }
}