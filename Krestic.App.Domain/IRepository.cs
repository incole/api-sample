﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.Domain
{
    public interface IRepository<T> 
    {
        Task<T> GetAsync(Guid id, CancellationToken cancellationToken);
        Task SaveAsync(T value);
    }
}