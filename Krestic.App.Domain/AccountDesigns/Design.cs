﻿using System;
using System.Collections.Generic;

namespace Krestic.Domain.AccountDesigns
{
    public class Design
    {
        public Guid Id { get; private set; }
        public Guid DesignerId { get; private set; }
        public bool IsDraft { get; private set; }
        public bool IsSellable { get; private set; }
        public bool IsHidden { get; private set; }
        public string Title { get; private set; }
        public string Collection { get; private set; }
        public decimal Price { get; private set; }
        public int? OrderNumber { get; private set; }
        public float ReviewScore { get; private set; }
        public int ReviewCount { get; private set; }
        public string Keywords { get; private set; }
        public string Icon { get; private set; }
        public int ColorCount { get; private set; }
        public int StitchWidth { get; private set; }
        public int StitchHeight { get; private set; }
        public int TotalStitches { get; private set; }
        public int FinishWidth { get; private set; }
        public int FinishHeight { get; private set; }
        public Designer Designer { get; private set; }
    }
}