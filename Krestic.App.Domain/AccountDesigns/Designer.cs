﻿using System;
using System.Collections.Generic;

namespace Krestic.Domain.AccountDesigns
{
    public class Designer
    {
        public Guid Id { get; private set; }
        public string Title { get; private set; }
    }
}