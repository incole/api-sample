using System;
using System.Collections.Generic;
using System.Linq;

namespace Krestic.Domain.AccountDesigns
{
    public class Account
    {
        public Guid Id { get; private set; }
        public bool IsActive { get; private set; }
        public Guid? DesignerId { get; private set; }
        public IEnumerable<Purchase> Purchases { get; set; }
    }
}