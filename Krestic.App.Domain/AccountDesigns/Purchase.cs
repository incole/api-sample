﻿using System;

namespace Krestic.Domain.AccountDesigns
{
    public class Purchase
    {
        public Guid Id { get; private set; }
        public Guid AccountId { get; private set; }
        public Guid DesignId { get; private set; }
        public Design Design { get; private set; }
    }
}