﻿using System;

namespace Krestic.App.Domain.Metrics
{
    public class AppExceptionMessage
    {
        public Guid Id { get; init; }
        public string Device { get; init; }
        public string DeviceId { get; init; }
        public string Os { get; init; }
        public string Release { get; init; }
        public string Email { get; init; }
        public string Message { get; init; }
        public DateTime Date { get; init; }

        public AppExceptionMessage(Guid id, string device, string deviceId, string os, string release, string email, string message)
        {
            Id = id;
            Device = device;
            DeviceId = deviceId;
            Os = os;
            Release = release;
            Email = Email;
            Message = message;
            Date = DateTime.UtcNow;
        }
    }
}