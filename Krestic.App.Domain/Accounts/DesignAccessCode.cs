﻿using System;

namespace Krestic.App.Domain.Accounts
{
    public class DesignAccessCode
    {
        public Guid Id { get; init; }
        public string Code { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsSpendable { get; set; }
        public bool IsUsed { get; set; }
    }
}