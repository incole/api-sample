﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Krestic.App.Domain.Accounts
{
    public class Purchase
    {
        public Guid Id { get; private set; }
        public Guid AccountId { get; private set; }
        public Guid DesignId { get; private set; }
        public decimal Amount { get; private set; }
        public decimal PlatformFee { get; private set; }
        public decimal Fee { get; private set; }
        public DateTime Date { get; private set; }
        public string Platform { get; private set; }
        public string ReceiptId { get; private set; }
        public string ReceiptRaw { get; private set; }
        public string ReceiptHash { get; private set; }

        internal Purchase() { }

        public Purchase(Guid id, Guid accountId, Guid designId, decimal amount, decimal platrofmFee, decimal fee, string platform,
            string receiptId, string receiptRaw)
        {
            Id = id;
            AccountId = accountId;
            DesignId = designId;
            ReceiptId = receiptId;
            ReceiptRaw = receiptRaw;
            Amount = amount;
            PlatformFee = platrofmFee;
            Platform = platform;
            Fee = fee;
            Date = DateTime.UtcNow;
            ReceiptHash = Convert.ToBase64String(SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(receiptRaw)));
        }
    }
}