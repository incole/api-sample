﻿using System;

namespace Krestic.App.Domain.Accounts
{
    public class DesignCode
    {
        public Guid Id { get; init; }
        public Guid DesignId { get; init; }
        public string Code { get; init; }
        public bool IsActive { get; private set; }
        public DateTime CreateDate { get; private set; }
        public bool UseOnce { get; private set; }
        public Guid? Recipient { get; private set; }
        public DateTime? ReceiveDate { get; private set; }
        public int RecipientCount { get; private set; }
        public virtual Design Design { get; private set; }

        internal DesignCode() {}

        public void Consume(Guid recipient)
        {
            RecipientCount++;

            if (!UseOnce)
                return;

            Recipient = recipient;
            ReceiveDate = DateTime.UtcNow;
        }
    }
}