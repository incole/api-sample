﻿using System;

namespace Krestic.App.Domain.Accounts
{
    public class DesignAccess
    {
        public Guid Id { get; init; }
        public Guid AccountId { get; init; }
        public Guid DesignId { get; init; }
        public string Reason { get; private set; }
        public Guid ReasonId { get; private set; }
        public bool IsActive { get; private set; }
        public Design Design { get; private set; }

        protected DesignAccess() { }

        internal DesignAccess(Guid id, Guid accountId, Guid designId, string reason, Guid reasonId)
        {
            Id = id;
            AccountId = accountId;
            DesignId = designId;
            Reason = reason;
            ReasonId = reasonId;
            IsActive = true;
        }
    }
}