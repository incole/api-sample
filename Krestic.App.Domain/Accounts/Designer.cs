﻿using System;
using System.Collections.Generic;

namespace Krestic.App.Domain.Accounts
{
    public class Designer
    {
        public Guid Id { get; init; }
        public Guid AccountId { get; init; }
        public string Title { get; private set; }

        public Designer(string title)
        {
            Id = Guid.NewGuid();
            Title = title;
        }

        public void SetTitle(string title)
        {
            Title = title;
        }
    }
}