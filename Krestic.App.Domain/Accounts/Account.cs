using System;
using System.Collections.Generic;

namespace Krestic.App.Domain.Accounts
{
    public class Account
    {
        public Guid Id { get; init; }
        public bool IsActive { get; private set; }
        public string Email { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public DateTime CreateDate { get; init; }
        public string Password { get; private set; }

        public IEnumerable<DesignAccess> DesignAccess => _designAccess;
        private IList<DesignAccess> _designAccess;

        public IEnumerable<Purchase> Purchases => _purchases;
        private IList<Purchase> _purchases;

        protected Account() { }

        public Account(Guid id, string email, string hashedPassword)
        {
            Id = id;
            Email = email;
            Password = hashedPassword;
            IsActive = true;
            CreateDate = DateTime.UtcNow;
        }

        public void AddPurchase(Guid designId, string receiptId, string receiptRaw, decimal amount, 
            decimal platrofmFee, decimal fee, string platform)
        {
            if (_purchases == null)
                _purchases = new List<Purchase>();

            var purchaseId = Guid.NewGuid();

            _purchases.Add(new Purchase(purchaseId, Id, designId, amount, platrofmFee, fee, platform, receiptId, receiptRaw));
            AddAccess(designId, "Purchase", purchaseId);
        }

        public void ConsumeCode(DesignCode code)
        {
            if (_designAccess == null)
                _designAccess = new List<DesignAccess>();

            code.Consume(Id);
            AddAccess(code.DesignId, "Code", code.Id);
        }

        public void AddFreeAccess(Guid designId)
        {
            AddAccess(designId, "Free", designId);
        }

        private void AddAccess(Guid designId, string reason, Guid reasonId)
        {
            if (_designAccess == null)
                _designAccess = new List<DesignAccess>();

            _designAccess.Add(new DesignAccess(Guid.NewGuid(), Id, designId, reason, reasonId));
        }
    }
}