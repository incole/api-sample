﻿using System;
using System.Collections.Generic;

namespace Krestic.App.Domain.Accounts
{
    public class Design
    {
        public Guid Id { get; private set; }
        public Guid DesignerId { get; private set; }
        public bool IsPublished { get; private set; }
        public bool IsListed { get; private set; }
        public bool IsHidden { get; private set; }
        public string Title { get; private set; }
        public decimal Price { get; private set; }
        public int? OrderNumber { get; private set; }
        public string FlossVendor { get; private set; }
        public int FlossCount { get; private set; }
        public int CanvasCount { get; private set; }
        public string CanvasType { get; private set; }
        public string CanvasColor { get; private set; }
        public int CanvasWidth { get; private set; }
        public int CanvasHeight { get; private set; }
        public int StitchWidth { get; private set; }
        public int StitchHeight { get; private set; }
        public int TotalStitches { get; private set; }
        public string Keywords { get; private set; }
        public string Icon { get; private set; }
        public virtual Designer Designer { get; private set; }
    }
}