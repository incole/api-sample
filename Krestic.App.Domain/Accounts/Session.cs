﻿using System;

namespace Krestic.App.Domain.Accounts
{
    public class Session
    {
        public Guid Id { get; private set; }
        public Guid AccountId { get; private set; }
        public DateTime ExpireDate { get; private set; }
        public string ClientId { get; private set; }
        public bool IsActive { get; private set; }

        protected Session() { }
    }
}