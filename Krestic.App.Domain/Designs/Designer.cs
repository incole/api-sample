﻿using System;

namespace Krestic.App.Domain.Designs
{
    public class Designer
    {
        public Guid Id { get; private set; }
        public string Title { get; private set; }
    }
}