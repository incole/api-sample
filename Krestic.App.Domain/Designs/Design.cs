﻿using System;
using System.Collections.Generic;

namespace Krestic.App.Domain.Designs
{
    public class Design
    {
        public Guid Id { get; private set; }
        public Guid DesignerId { get; private set; }
        public bool IsPublished { get; private set; }
        public bool IsListed { get; private set; }
        public bool IsHidden { get; private set; }
        public string Title { get; private set; }
        public string Collection { get; private set; }
        public decimal Price { get; private set; }
        public int? OrderNumber { get; private set; }
        public string Keywords { get; private set; }
        public string Icon { get; private set; }
        public string FlossVendor { get; private set; }
        public int FlossCount { get; private set; }
        public int CanvasCount { get; private set; }
        public string CanvasType { get; private set; }
        public string CanvasColor { get; private set; }
        public int CanvasWidth { get; private set; }
        public int CanvasHeight { get; private set; }
        public int StitchWidth { get; private set; }
        public int StitchHeight { get; private set; }
        public int TotalStitches { get; private set; }
        public string Theme { get; private set; }
        public bool HasBackStitches { get; private set; }
        public bool HasHalfStitches { get; private set; }
        public bool HasQuarterStitches { get; private set; }
        public bool HasPetitStitches { get; private set; }
        public bool HasFrenchKnots { get; private set; }
        public bool HasBlends { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }
        public DateTime PublishDate { get; private set; }
        public Designer Designer { get; private set; }
        public virtual ICollection<DesignImage> Images { get; private set; }
    }
}