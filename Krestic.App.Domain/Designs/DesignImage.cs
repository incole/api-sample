﻿using System;

namespace Krestic.App.Domain.Designs
{
    public class DesignImage
    {
        public Guid Id { get; private set; }
        public Guid DesignId { get; private set; }
        public string Url { get; private set; }
        public int Order { get; private set; }
    }
}