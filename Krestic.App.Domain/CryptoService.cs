﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Krestic.App.Domain
{
    public class CryptoService
    {
        public byte[] Encrypt(byte[] data, string encryptionKey)
        {
            byte[] clearBytes = data;
            using Aes encryptor = Aes.Create();

            var IV = new byte[15];
            var rand = new Random();

            rand.NextBytes(IV);

            var pdb = new Rfc2898DeriveBytes(encryptionKey, IV);
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);

            using MemoryStream ms = new MemoryStream();
            using CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write);

            cs.Write(clearBytes, 0, clearBytes.Length);
            cs.Close();

            var result = new List<byte>();
            result.AddRange(IV);
            result.AddRange(ms.ToArray());

            return result.ToArray();
        }

        public byte[] Decrypt(byte[] encrypted, string encryptionKey)
        {
            var IV = encrypted[..15];
            var cipherBytes = encrypted[15..];
            using var encryptor = Aes.Create();

            var pdb = new Rfc2898DeriveBytes(encryptionKey, IV);
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);

            using var ms = new MemoryStream();
            using var cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write);

            cs.Write(cipherBytes, 0, cipherBytes.Length);
            cs.Close();

            return ms.ToArray();
        }
    }
}