using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Krestic.App.Infrastructure.Accounts.Queries;
using System;
using System.Threading.Tasks;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class AuthorizeAttribute : Attribute, IAsyncAuthorizationFilter
{
    public async Task OnAuthorizationAsync(AuthorizationFilterContext actionContext)
    {
        if (!actionContext.HttpContext.Request.Headers.ContainsKey("Authorization"))
        {
            actionContext.Result = new StatusCodeResult(401);
            return;
        }

        var authorization = actionContext.HttpContext.Request.Headers["Authorization"][0];
        
        var rawData = authorization.Split();
        if (rawData.Length < 2)
        {
            actionContext.Result = new StatusCodeResult(401);
            return;
        }

        if(!Guid.TryParse(rawData[1], out var sessionKey))
        {
            actionContext.Result = new StatusCodeResult(401);
            return;
        }

        var mediator = actionContext.HttpContext.RequestServices.GetService(typeof(IMediator)) as IMediator;

        var session = await mediator.Send(new SessionQuery(sessionKey));
        if (session == null || !session.IsActive || session.ExpireDate < DateTime.UtcNow)
        {
            actionContext.Result = new StatusCodeResult(401);
            return;
        }
        
        actionContext.HttpContext.Items.Add("Session", session);
        actionContext.HttpContext.Items.Add("Account", session.AccountId);
    }
}