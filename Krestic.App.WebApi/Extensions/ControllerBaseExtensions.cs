﻿using Microsoft.AspNetCore.Mvc;
using Krestic.App.Infrastructure.Accounts.Queries;

namespace Krestic.App.WebApi.Extensions
{
    public static class ControllerBaseExtensions
    {
        public static SessionQueryResult GetUserSession(this ControllerBase controllerBase)
        {
            return controllerBase.Request.HttpContext.Items["Session"] as SessionQueryResult;
        }
    }
}
