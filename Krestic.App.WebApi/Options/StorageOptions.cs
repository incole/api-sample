﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Krestic.App.WebApi.Options
{
    public class StorageOptions
    {
        public string Patterns { get; set; }
    }
}
