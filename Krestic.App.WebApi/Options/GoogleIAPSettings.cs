﻿namespace Krestic.App.WebApi.Options
{
    public class GoogleIAPSettings
    {
        public string ApplicationName { get; set; }
        public decimal Fee { get; set; }
        public string KeyFile { get; set; }
        public string Package { get; set; }
    }
}
