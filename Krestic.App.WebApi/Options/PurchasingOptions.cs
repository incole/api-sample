﻿namespace Krestic.App.WebApi.Options
{
    public class PurchasingOptions
    {
        public GoogleIAPSettings Google { get; set; }
        public decimal Fee { get; set; }
    }
}
