﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Krestic.App.Infrastructure;
using Krestic.App.Infrastructure.Designs;
using Krestic.App.Infrastructure.Designs.Queries;
using Krestic.App.WebApi.Contract;
using Krestic.App.WebApi.Contract.Designs;
using Krestic.Http.Extensions;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.WebApi.Controllers
{
    [ApiController]
    public class DesignsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public DesignsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("/designs/")]
        [ProducesResponseType(200, Type = typeof(Page<DesignSimpleView>))]
        [Log(false, false)]
        public async Task<IActionResult> GetDesigns([FromQuery] int page = 1, [FromQuery] int limit = 30, 
            [FromQuery] string designer = null, [FromQuery] string theme = null, [FromQuery] string searchString = null,
            [FromQuery] DesignsListOrder orderBy = DesignsListOrder.Common,
            CancellationToken cancellationToken = default)
        {
            if (theme == "Featured")
                return await GetFeaturedDesigns(page, limit, cancellationToken);

            var query = new DesignsQuery(page, limit, designer, theme, searchString, null) { Order = orderBy };
            var result = await _mediator.Send(query, cancellationToken);
            
            return Ok(result);
        }

        [HttpGet("/designs/{id}")]
        [ProducesResponseType(200, Type = typeof(DesignCompleteView))]
        [Log(false, false)]
        public async Task<IActionResult> GetDesign([FromRoute] Guid id, CancellationToken cancellationToken = default)
        {
            var query = new DesignQuery(id);
            var result = await _mediator.Send(query, cancellationToken);

            if (result == null)
                return NotFound();

            return Ok(result);
        }


        [Log(false, false)]
        [HttpGet("/designs/themes")]
        [ProducesResponseType(200, Type = typeof(string[]))]
        public async Task<IActionResult> GetDesignThemes(CancellationToken cancellationToken = default)
        {
            return Ok(await _mediator.Send(new DesignThemesQuery(), cancellationToken));
        }

        [Log(false, false)]
        [HttpGet("/designs/compilations/free")]
        [ProducesResponseType(200, Type = typeof(Page<DesignSimpleView>))]
        public async Task<IActionResult> GetFreeDesigns([FromQuery] int page = 1, [FromQuery] int limit = 30,
            CancellationToken cancellationToken = default)
        {
            return Ok(await _mediator.Send(new FreeDesignsQuery(page, limit), cancellationToken));
        }

        [Log(false, false)]
        [HttpGet("/designs/compilations/beginners")]
        [ProducesResponseType(200, Type = typeof(Page<DesignSimpleView>))]
        public async Task<IActionResult> GetSimpleDesigns([FromQuery] int page = 1, [FromQuery] int limit = 30,
            CancellationToken cancellationToken = default)
        {
            return Ok(await _mediator.Send(new SimpleDesignsQuery(page, limit), cancellationToken));
        }

        [Log(false, false)]
        [HttpGet("/designs/compilations/featured")]
        [ProducesResponseType(200, Type = typeof(Page<DesignSimpleView>))]
        public async Task<IActionResult> GetFeaturedDesigns([FromQuery] int page = 1, [FromQuery] int limit = 30,
            CancellationToken cancellationToken = default)
        {
            return Ok(await _mediator.Send(new FeaturedDesignsQuery(page, limit), cancellationToken));
        }

        [Log(false, false)]
        [HttpGet("/designs/compilations/advanced")]
        [ProducesResponseType(200, Type = typeof(Page<DesignSimpleView>))]
        public async Task<IActionResult> GetChallengingDesigns([FromQuery] int page = 1, [FromQuery] int limit = 30,
            CancellationToken cancellationToken = default)
        {
            return Ok(await _mediator.Send(new ChallengingDesignsQuery(page, limit), cancellationToken));
        }

        [Log(false, false)]
        [HttpGet("/designs/compilations/new")]
        [ProducesResponseType(200, Type = typeof(Page<DesignSimpleView>))]
        public async Task<IActionResult> GetNewDesigns(CancellationToken cancellationToken = default)
        {
            var query = new DesignsQuery(1, 10, null, null, null, DateTime.UtcNow.Date.AddDays(-7)) { Order = DesignsListOrder.Random };
            return Ok(await _mediator.Send(query, cancellationToken));
        }

    }
}
