﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Krestic.App.Domain;
using Krestic.App.Domain.Metrics;
using Krestic.App.Infrastructure;
using Krestic.App.Infrastructure.Designs;
using Krestic.App.WebApi.Contract;
using Krestic.App.WebApi.Contract.Metrics;
using Krestic.Http.Extensions;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Krestic.App.WebApi.Controllers
{
    [ApiController]
    public class MetricsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MetricsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("/metrics/app/exceptions")]
        [ProducesResponseType(201)]
        [ProducesResponseType(204)]
        [Log(false, false)]
        public async Task<IActionResult> AddAppExceptionMessage([FromBody] AppExceptionMessageBinding binding,
            [FromServices] IRepository<AppExceptionMessage> repository,
            CancellationToken cancellationToken = default)
        {
            var note = await repository.GetAsync(binding.Id, cancellationToken);
            if (note != null)
                return NoContent();

            note = new AppExceptionMessage(binding.Id, binding.Device, binding.DeviceId, binding.Os, binding.Release, binding.Email, binding.Message);

            await repository.SaveAsync(note);

            return StatusCode(201);
        }
    }
}
