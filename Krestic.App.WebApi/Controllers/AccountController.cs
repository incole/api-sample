﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Krestic.App.WebApi.Extensions;
using System;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Extensions.Options;
using Krestic.App.WebApi.Options;
using System.IO;
using System.Linq;
using System.Text;
using Google.Apis.Auth.OAuth2;
using Google.Apis.AndroidPublisher.v3;
using Google.Apis.Services;
using Krestic.App.WebApi.Contract.Accounts;
using Krestic.App.Infrastructure.Accounts.Queries;
using Krestic.App.WebApi.Contract;
using Krestic.App.Domain.Accounts;
using Krestic.App.Domain;
using Krestic.App.Infrastructure.IAP;
using System.Security.Cryptography;
using Krestic.App.Infrastructure.Designs;
using Krestic.App.Infrastructure.Accounts;
using Krestic.Http.Extensions;

namespace Krestic.App.WebApi.Controllers
{
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AccountController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [Authorize]
        [HttpGet("/account/profile")]
        [Log(false, false)]
        [ProducesResponseType(200, Type = typeof(Contract.Accounts.AccountView))]
        public async Task<IActionResult> GetProfile()
        {
            var session = this.GetUserSession();
            var query = new AccountQuery(id: session.AccountId);
            var account = await _mediator.Send(query);

            return base.Ok(new Contract.Accounts.AccountView
            {
                Email = account.Email,
                CreateDate = account.CreateDate,
                FirstName = account.FirstName,
                LastName = account.LastName
            });
        }

        [Authorize]
        [HttpGet("/account/designs")]
        [Log(false, false)]
        [ProducesResponseType(200, Type = typeof(Page<DesignView>))]
        public async Task<IActionResult> GetDesigns([FromQuery] int page = 1, [FromQuery] int limit = 100, CancellationToken cancellationToken = default)
        {
            var session = this.GetUserSession();
            var query = new AccountDesignsQuery(session.AccountId, page, limit);

            return Ok(await _mediator.Send(query, cancellationToken));
        }

        [Authorize]
        [HttpPost("/account/designs")]
        [Log(false, false)]
        [ProducesResponseType(204)]
        public async Task<IActionResult> SaveDesign([FromBody] AddFreeDesignBinding model, [FromServices] IRepository<Account> repository)
        {
            var session = this.GetUserSession();
            var account = await repository.GetAsync(session.AccountId, CancellationToken.None);

            // Дизайн уже есть в коллекции
            if (account.DesignAccess.Any(x => x.DesignId == model.DesignId))
                return NoContent();

            var design = await _mediator.Send(new DesignQuery(model.DesignId));
            if (design == null)
                return BadRequest($"Design {model.DesignId} not found");

            if (design.Price != 0)
                return BadRequest($"Design {model.DesignId} is not free");

            account.AddFreeAccess(design.Id);
            await repository.SaveAsync(account);

            return NoContent();
        }

        [Authorize]
        [HttpGet("/account/designs/{id}")]
        [ProducesResponseType(200, Type = typeof(DesignView))]
        [Log(false, false)]
        public async Task<IActionResult> GetDesign([FromRoute] Guid id, CancellationToken cancellationToken = default)
        {
            var session = this.GetUserSession();
            var query = new AccountDesignQuery(session.AccountId, id);
            var design = await _mediator.Send(query, cancellationToken);

            if (design == null)
                return NotFound();

            return Ok(design);
        }

        [Authorize]
        [HttpGet("/account/designs/{id}/pattern")]
        [Log(false, false)]
        public async Task<IActionResult> GetPattern([FromRoute] Guid id,
            [FromServices] IOptions<CryptoOptions> cryptoOptions,
            [FromServices] IOptions<StorageOptions> storageOptions, 
            [FromServices] CryptoService cryptoService,
            CancellationToken cancellationToken = default)
        {
            var session = this.GetUserSession();
            var accountDesignQuery = new AccountDesignQuery(session.AccountId, id);

            var design = await _mediator.Send(accountDesignQuery, cancellationToken);
            if (design == null)
                return NotFound();

            var accountQuery = new AccountQuery(id: session.AccountId);
            var account = await _mediator.Send(accountQuery, cancellationToken);

            var patternFilePath = Path.Combine(storageOptions.Value.Patterns, $"{id}.ekp");

            if (!System.IO.File.Exists(patternFilePath))
                return NotFound();

            var encryptedData = System.IO.File.ReadAllBytes(patternFilePath);
            var data = cryptoService.Decrypt(encryptedData, cryptoOptions.Value.Key);
            var cryptedData = cryptoService.Encrypt(data, $"{account.Email}{session.ClientId}");

            var stream = new MemoryStream(cryptedData);

            return File(stream, "multipart/byteranges");
        }

        [HttpGet("/account/designs/codes/")]
        [ProducesResponseType(200, Type = typeof(DesignCodeView))]
        [Log(false, true)]
        public async Task<IActionResult> GetDesignCode([FromQuery] string code, CancellationToken cancellationToken = default)
        {
            var query = new DesignCodeQuery(code);
            var codeView = await _mediator.Send(query, cancellationToken);

            if (codeView == null)
                return NotFound();

            if (!codeView.IsActive)
                return NotFound();

            if (codeView.IsConsumed)
                return Conflict();
            
            var result = new DesignCodeView
            {
                Id = codeView.Id,
                Code = codeView.Code,
                Design = codeView.Design
            };

            return Ok(result);
        }

        [Authorize]
        [HttpPost("/account/designs/codes/{codeId}/consume")]
        [ProducesResponseType(204)]
        [Log(true, true)]
        public async Task<IActionResult> ConsumeDesignCode([FromRoute] Guid codeId, 
            [FromServices] IRepository<Account> accountRepository, [FromServices] IRepository<DesignCode> designCodeRepository,
            [FromServices] DesignCodeService designCodeService, CancellationToken cancellationToken = default)
        {
            var session = this.GetUserSession();
            
            var account = await accountRepository.GetAsync(session.AccountId, cancellationToken);
            var designCode = await designCodeRepository.GetAsync(codeId, cancellationToken);
            if (designCode == null)
                return NotFound();

            if (designCode.Design.IsHidden || !designCode.Design.IsListed)
                return NotFound();

            // Код уже использован
            if (designCode.Recipient != null)
                return BadRequest();

            // У пользователя уже есть этот дизайн
            if (account.DesignAccess != null)
            {
                if (account.DesignAccess.Any(x => x.DesignId == designCode.DesignId))
                    return Conflict();
            }

            await designCodeService.ConsumeDesignCode(account, designCode, cancellationToken);

            return NoContent();
        }

        [Authorize]
        [HttpPost("/account/purchases/google")]
        [Log(true, true)]
        public async Task<IActionResult> AddPurchase([FromBody] PurchaseBinding model,
                                                     [FromServices] IRepository<Account> repository,
                                                     [FromServices] IOptions<PurchasingOptions> purchasingOptions,
                                                     CancellationToken cancellationToken = default)
        {
            var accountId = this.GetUserSession().AccountId;
            var purchaseHash = Convert.ToBase64String(SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(model.Payload)));

            // Покупка уже есть в системе
            var purchaseView = await _mediator.Send(new PurchaseQuery(purchaseHash), cancellationToken);
            if (purchaseView != null)
            {
                if (purchaseView.AccountId == accountId &&
                    purchaseView.DesignId == model.DesignId)
                    return NoContent();
                
                return Conflict();
            }

            var account = await repository.GetAsync(accountId, cancellationToken);

            // Дизайн уже есть в коллекции
            if (account.DesignAccess.Any(x => x.DesignId == model.DesignId))
                return BadRequest($"Design {model.DesignId} already in collection");

            var design = await _mediator.Send(new DesignQuery(model.DesignId));
            if (design == null)
                return BadRequest($"Design {model.DesignId} not found");

            var payloadData = Encoding.UTF8.GetString(Convert.FromBase64String(model.Payload));

            var amount = design.Price;
            var platformFee = Math.Round(amount / 100 * purchasingOptions.Value.Google.Fee, 2, MidpointRounding.ToPositiveInfinity);
            var fee = Math.Round(amount / 100 * purchasingOptions.Value.Fee, 2, MidpointRounding.ToPositiveInfinity);

            var receipt = GoogleReceiptParcer.Parce(payloadData);
            var credential = GoogleCredential.FromFile(Path.Combine(purchasingOptions.Value.Google.KeyFile));
            var service = new AndroidPublisherService(new BaseClientService.Initializer() { HttpClientInitializer = credential, ApplicationName = purchasingOptions.Value.Google.ApplicationName });

            var googleReceipt = await service.Purchases.Products.Get(purchasingOptions.Value.Google.Package, receipt.productId, receipt.purchaseToken).ExecuteAsync();
            
            if (googleReceipt.PurchaseState != 0)
                return BadRequest($"Invalid purchase state: {googleReceipt.PurchaseState}"); //todo:

            //purchase state of the order. Possible values are: 0. Purchased 1. Canceled 2. Pending
            //The acknowledgement state of the inapp product. Possible values are: 0. Yet to be acknowledged 1. Acknowledged

            account.AddPurchase(model.DesignId, receipt.purchaseToken, model.Payload, amount, platformFee, fee, "Google Play");
            await repository.SaveAsync(account);

            return new StatusCodeResult(201);
        }
    }

}
