using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using MediatR;
using Krestic.App.Infrastructure.Accounts;
using Krestic.App.Domain.Accounts;
using Krestic.App.Domain.Metrics;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;
using Krestic.App.Domain;
using Krestic.App.Infrastructure.Designs;
using Krestic.App.WebApi.Options;
using Microsoft.AspNetCore.HttpOverrides;
using Krestic.Http.Extensions;
using Krestic.App.Infrastructure.Metrics;

namespace Krestic.App.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.UseFileLogging(Configuration);

            services.Configure<StorageOptions>(Configuration.GetSection("Storage"));
            services.Configure<PurchasingOptions>(Configuration.GetSection("Purchasing"));

            services.AddMvc()
                    .ConfigureApiBehaviorOptions(options =>
                    {
                        options.SuppressMapClientErrors = true;
                    })
                    .AddJsonOptions(jsonOptions => { jsonOptions.JsonSerializerOptions.PropertyNamingPolicy = null; });

            services.AddSwaggerGen(c =>
            {
                c.CustomSchemaIds(type => type.ToString());
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Krestik App API", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer xxxxx-xxxxx-xxxxx-xxxxx-xxxxx'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        { 
                            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header
                        },
                        new List<string>()
                    }
                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddDbContextWithConnection<AccountsDbContext>(Configuration, "Krestic");
            services.AddDbContextWithConnection<DesignsDbContext>(Configuration, "Krestic");
            services.AddDbContextWithConnection<MetricsDbContext>(Configuration, "Krestic");

            services.AddScoped<DesignCodeService>();
            services.AddScoped<CryptoService>();
            services.AddScoped<IRepository<Account>, AccountRepository>();
            services.AddScoped<IRepository<Session>, SessionRepository>();
            services.AddScoped<IRepository<DesignCode>, DesignCodeRepository>();
            services.AddScoped<IRepository<AppExceptionMessage>, AppExceptionMessageRepository>();

            services.AddMediatR(typeof(AccountRepository).Assembly);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor |
                ForwardedHeaders.XForwardedProto
            });

            app.UseLogginig();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.yaml", "My API V1");
            });

            app.UseCors(cors => cors
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true)
                .AllowCredentials()
                );

            app.UseRouting();

            app.UseEndpoints(x => x.MapControllers());
        }
    }
}
