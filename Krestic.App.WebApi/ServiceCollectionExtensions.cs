﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Reflection;

namespace Krestic.App.WebApi
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Инициализирует DbContext с конкретной строкой подключения
        /// </summary>
        public static IServiceCollection AddDbContextWithConnection<TContext>(this IServiceCollection services, IConfiguration configuration, string connectionString) where TContext : DbContext
        {
            return services.AddDbContext<TContext>(
                opts => opts.UseNpgsql(configuration.GetConnectionString(connectionString))
            );
        }

        public static void RegisterAllTypes<T>(this IServiceCollection services, Assembly[] assemblies, ServiceLifetime lifetime = ServiceLifetime.Transient)
        {
            var typesFromAssemblies = assemblies.SelectMany(a => a.DefinedTypes.Where(x => x.GetInterfaces().Contains(typeof(T))));
            
            foreach (var type in typesFromAssemblies)
                services.Add(new ServiceDescriptor(typeof(T), type, lifetime));
        }
    }
}
